create table category (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  urlname                       varchar(255),
  constraint uq_category_name unique (name),
  constraint uq_category_urlname unique (urlname),
  constraint pk_category primary key (id)
);

create table chapter (
  id                            bigint auto_increment not null,
  index_chapter                 bigint,
  name                          varchar(255),
  content                       TEXT,
  comic_id                      bigint,
  constraint pk_chapter primary key (id)
);

create table comic (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  urlname                       varchar(255),
  author                        varchar(255),
  source                        varchar(255),
  status                        varchar(255),
  rate                          float,
  constraint uq_comic_name unique (name),
  constraint uq_comic_urlname unique (urlname),
  constraint pk_comic primary key (id)
);

create table comic_category (
  comic_id                      bigint not null,
  category_id                   bigint not null,
  constraint pk_comic_category primary key (comic_id,category_id)
);

create index ix_chapter_comic_id on chapter (comic_id);
alter table chapter add constraint fk_chapter_comic_id foreign key (comic_id) references comic (id) on delete restrict on update restrict;

create index ix_comic_category_comic on comic_category (comic_id);
alter table comic_category add constraint fk_comic_category_comic foreign key (comic_id) references comic (id) on delete restrict on update restrict;

create index ix_comic_category_category on comic_category (category_id);
alter table comic_category add constraint fk_comic_category_category foreign key (category_id) references category (id) on delete restrict on update restrict;

