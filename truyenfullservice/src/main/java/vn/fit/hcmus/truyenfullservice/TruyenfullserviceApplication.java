package vn.fit.hcmus.truyenfullservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"vn.fit.hcmus.truyenfullservice"})
public class TruyenfullserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TruyenfullserviceApplication.class, args);
    }

}
