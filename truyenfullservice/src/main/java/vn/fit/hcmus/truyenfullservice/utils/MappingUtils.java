package vn.fit.hcmus.truyenfullservice.utils;

import org.dozer.DozerBeanMapper;
import vn.fit.hcmus.truyenfullservice.thrift.generated.TComic;

import java.util.ArrayList;
import java.util.List;

public class MappingUtils {


    public static TComic thrift2db(TComic thrift) {
        TComic ms = new TComic(thrift.getId(),thrift.getName(),thrift.getUrlname(),thrift.getAuthor(),thrift.getSource(),thrift.getStatus(),thrift.getRate());
        return ms;
    }

    private static DozerBeanMapper mapper = new DozerBeanMapper();

    public static <E, T> E convertObject(T input, Class<E> clazz) {
        return mapper.map(input, clazz);
    }

    public static <E, T> List<E> convertList(List<T> input, Class<E> clazz) {
        List<E> listModel = new ArrayList<>();
        for (T t : input) {
            listModel.add(mapper.map(t, clazz));
        }
        return listModel;
    }
}
